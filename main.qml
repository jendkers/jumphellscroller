import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.5

import Box2D 2.0

import "Sensors" as Sensors
import "Platforms" as Platforms
import "Collectibles" as Collectibles

import "MyEngine.js" as Engine

Window {
    id: app

    visible: true

    width: 1000
    height: 480

    title: qsTr("Jump scroller")

    property bool isUpKeyPressed: false
    property bool isRightKeyPressed: false

    property string appState: appStates.view

    /** interval for placing items on world */
    property int gridXValue: 32
    property int gridYValue: 32

    Component.onCompleted: container.forceActiveFocus()

    QtObject {
        id: appStates
        property string view: "STATE_VIEW"
        property string play: "STATE_PLAY"
        property string add: "STATE_ADD"
        property string del: "STATE_DEL"
    }

    Item {
        id: container

        anchors.fill: parent

        Rectangle {
            anchors.fill: parent
            gradient: Gradient {
                GradientStop { color: "lightblue"; position: 0.6 }
                GradientStop { color: "white"; position: 1.0 }
            }
        }

        Keys.onPressed: {
            if (event.key === Qt.Key_Up && event.isAutoRepeat === false) {
                player.keyUpPressed(event)
            } else if (event.key === Qt.Key_Right) {
                player.keyRight()
            } else if (event.key === Qt.Key_Left) {
                player.keyLeft()
            } else if (event.key === Qt.Key_Escape) {
                Qt.quit()
            }
        }

        Keys.onReleased: {
            if (event.key === Qt.Key_Up && event.isAutoRepeat === false)
            {
                player.keyUpReleased(event)
            }
            else if (event.key === Qt.Key_Left || event.key === Qt.Key_Right)
            {
                // stops player velocity when user releases arrows
                var playerVelx = player.body.linearVelocity.x
                if (playerVelx > -2 && playerVelx != 0) {
                    player.body.linearVelocity.x = 1
                } else if (playerVelx < 2 && playerVelx != 0) {
                    player.body.linearVelocity.x = -1
                }
            }
        }
    }

    Flickable {
        id: flickable

        width: app.width
        height: app.height

        contentWidth: app.width * 2
        contentHeight: app.height * 2

        Sensors.SensorWall {
            id: sensorWallRight

            width: app.width / 3
            height: app.height

            active: !flickable.dragging

            boxObj.x: flickable.contentX + app.width - width
            boxObj.y: flickable.contentY

            world: b2dPhysicsWorld

            onContactWithPlayer: {
                if (player.body.linearVelocity.x > 0 && flickable.contentX < flickable.contentWidth-sensorWallRight.width)
                    flickable.contentX = player.x - (app.width-sensorWallRight.width-player.box.width)
            }
        }

        Sensors.SensorWall {
            id: sensorWallLeft

            width: app.width / 3
            height: app.height

            active: !flickable.dragging

            boxObj.x: flickable.contentX
            boxObj.y: flickable.contentY

            world: b2dPhysicsWorld

            onContactWithPlayer: {
                if (player.body.linearVelocity.x < 0 && flickable.contentX > 0)
                    flickable.contentX = player.x - sensorWallLeft.width
            }
        }

        Sensors.SensorWall {
            id: sensorWallTop

            width: app.width
            height: app.height / 7

            active: !flickable.dragging

            boxObj.x: flickable.contentX
            boxObj.y: flickable.contentY

            world: b2dPhysicsWorld

            onContactWithPlayer: {
                if (player.body.linearVelocity.y < 0)
                    flickable.contentY = player.y - sensorWallTop.height
            }
        }

        Sensors.SensorWall {
            id: sensorWallBottom

            width: app.width
            height: app.height / 7

            active: !flickable.dragging

            boxObj.x: flickable.contentX
            boxObj.y: flickable.contentY + app.height - sensorWallBottom.height

            world: b2dPhysicsWorld

            onContactWithPlayer: {
                if (player.body.linearVelocity.y > 0)
                    flickable.contentY = player.y - (app.height - sensorWallBottom.height - player.height)
            }
        }

        World {
            id: b2dPhysicsWorld
        }

        DebugDraw {
            visible: true
        }

//        Platforms.BlockPlatform {
//            width: parent.width
//            wantedWidth: parent.width
//            wantedHeight: 50

//            x: 200
//            y: 400
//        }

//        Platforms.BlockPlatform {
//            width: parent.width / 6
//            wantedWidth: parent.width / 6
//            wantedHeight: 50
//            anchors {
//                verticalCenter: parent.verticalCenter
//                left: parent.left
//            }
//        }

        MouseArea {
            id: maAddingPlatform

            anchors.fill: parent

            enabled: flickable.interactive === false

            property real pressPosX: 0
            property real pressPosY: 0
            property Platforms.Platform createdPlatform

            onPressed: {
                pressPosX = mouseX
                pressPosY = mouseY

                if (appState == appStates.add) {
                    if (btnAddPlatform.checked) {
                        createdPlatform = platformFactoryObj.createPlatform(pressPosX,
                                                                            mouseY,
                                                                            32,
                                                                            32,
                                                                            platformFactoryObj.typeOfPlatform)
                    } else if (btnAddCollectible.checked) {
                        collectibleFactory.createCollectable(pressPosX,
                                                          mouseY,
                                                          collectibleFactory.typeOfCollectible)
                    } else if (btnAddObjects.checked) {
                        createdPlatform = inactiveObjectsFactory.createObject(pressPosX,
                                                                              mouseY,
                                                                              32,
                                                                              32,
                                                                              inactiveObjectsFactory.typeOfObject)
                    } else if (btnSetPlayer.checked) {
                        player.x = mouseX
                        player.y = mouseY
                    }
                } else if (appState == appStates.del) {
                    var obj
                    obj = inactiveObjectsFactory.childAt(mouseX, mouseY)
                    if (obj) {
                        if (obj.objectName === "platform" || obj.objectName === "inactiveObject") {
                            obj.destroy()
                        }
                    }
                    obj = platformFactoryObj.childAt(mouseX, mouseY)
                    if (obj) {
                        if (obj.objectName === "platform") {
                            obj.destroy()
                        }
                    }
                    obj = collectibleFactory.childAt(mouseX, mouseY)
                    if (obj) {
                        if (obj.objectName === "collectible") {
                            obj.destroy()
                        }
                    }
                }
            }

            onPositionChanged: {
                if (containsPress && appState == appStates.add && (btnAddPlatform.checked || btnAddObjects.checked)) {
                    createdPlatform.wantedWidth = mouseX - pressPosX
                    createdPlatform.wantedHeight = mouseY - pressPosY
                }
            }
        }

        InactiveObjectsFactory {
            id: inactiveObjectsFactory

            anchors.fill: parent

            windowWidth: app.width
            windowHeight: app.height
            world: b2dPhysicsWorld

            property string typeOfObject: ""
        }

        PlatformFactory {
            id: platformFactoryObj

            anchors.fill: parent

            windowWidth: app.width
            windowHeight: app.height
            world: b2dPhysicsWorld

            property string typeOfPlatform: ""
        }


        CollectibleFactory {
            id: collectibleFactory

            anchors.fill: parent

            world: b2dPhysicsWorld

            property string typeOfCollectible: ""
        }

        Player {
            id: player

            x: player.width * 20

            world: b2dPhysicsWorld

            visible: false
            body.active: false

            heroTheme: "hero"
        }
    }

    Row {
        id: rowEditorButtons

        RadioButton {
            id: btnPlay

            text: "play"

            checked: true

            onClicked: {
                flickable.interactive = !flickable.interactive
                setApplicationState(appStates.play)

                container.forceActiveFocus()
                container.focus = true
            }
        }

        RadioButton {
            id: btnView

            text: "view"

            onClicked: {
                flickable.interactive = true
                setApplicationState(appStates.view)
            }
        }

        RadioButton {
            id: btnAddPlatform

            text: "add platforms"

            onClicked: {
                flickable.interactive = false
                setApplicationState(appStates.add)
            }

            ListView {
                id: columnPlatforms

                width: parent.width
                height: 50 * count

                anchors {
                    left: parent.left
                    top: parent.bottom
                    horizontalCenter: parent.horizontalCenter
                }

                model: platformFactoryObj.model

                visible: btnAddPlatform.checked

                delegate: RadioButton {
                    text: typeRole
                    onClicked: platformFactoryObj.typeOfPlatform = typeRole
                }
            }
        }

        RadioButton {
            id: btnAddCollectible

            text: "add collectible"

            onClicked: {
                flickable.interactive = false
                setApplicationState(appStates.add)
                Engine.printChildrens(collectibleFactory.children)
            }

            ListView {
                id: columnCollectible

                width: parent.width
                height: 50 * count

                anchors {
                    left: parent.left
                    top: parent.bottom
                    horizontalCenter: parent.horizontalCenter
                }

                model: collectibleFactory.model

                visible: btnAddCollectible.checked

                delegate: RadioButton {
                    text: typeRole
                    onClicked: collectibleFactory.typeOfCollectible = typeRole
                }
            }
        }

        RadioButton {
            id: btnAddObjects

            text: "add objects"

            onClicked: {
                flickable.interactive = false
                setApplicationState(appStates.add)
            }

            ListView {
                id: columnInactiveObjects

                width: parent.width
                height: 50 * count

                anchors {
                    left: parent.left
                    top: parent.bottom
                    horizontalCenter: parent.horizontalCenter
                }

                model: inactiveObjectsFactory.model

                visible: btnAddObjects.checked

                delegate: RadioButton {
                    text: typeRole
                    onClicked: inactiveObjectsFactory.typeOfObject = typeRole
                }
            }
        }

        RadioButton {
            id: btnDeletePlatform

            text: "del"

            onClicked: {
                flickable.interactive = false
                setApplicationState(appStates.del)
            }
        }

        RadioButton {
            id: btnSetPlayer

            text: "Set player"

            onClicked: {
                flickable.interactive = false
                setApplicationState(appStates.add)
                player.visible = true
                player.body.active = true
            }
        }

        Button {
            id: btnSaveLevel
            text: "Save level"

            onClicked: cppLevelController.saveLevel(
                           Engine.getLevelData(
                               flickable,
                               player,
                               inactiveObjectsFactory.children,
                               platformFactoryObj.children,
                               collectibleFactory.children))
        }
        Button {
            id: btnLoadLevel
            text: "Load level"

            onClicked: Engine.buildLevel(cppLevelController.loadLevel(1),
                                         flickable,
                                         player,
                                         inactiveObjectsFactory,
                                         platformFactoryObj,
                                         collectibleFactory)
        }
        Button {
            id: btnResetLevel
            text: "Reset level"

            onClicked: Engine.resetLevel(flickable,
                                         player,
                                         inactiveObjectsFactory.children,
                                         platformFactoryObj.children,
                                         collectibleFactory.children)
        }
    }

    /**
     * @brief Sets application to state
     * @param type:string state application state to set
     */
    function setApplicationState(state)
    {
        app.appState = state
    }
}
