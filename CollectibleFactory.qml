import QtQuick 2.9

import Box2D 2.0

import "MyEngine.js" as Engine

import "Collectibles" as Collectibles

Item {
    id: collectibleFactory

    anchors.fill: parent

    property World world

    property var component
    property var collectibleObject

    property alias model: lmCollectibles

    ListModel {
        id: lmCollectibles
        ListElement { typeRole: "coin"; pathRole: "Collectibles/Coin.qml" }
        ListElement { typeRole: "powerup"; pathRole: "Collectibles/PowerUp.qml" }
    }

    function getRandomArbitrary(min, max) {
      return Math.random() * (max - min) + min;
    }

    function createCollectable(x,y,collectibleType) {
        var roundedValue = Engine.getRoundedGridValue(x, y, app.gridXValue, app.gridYValue)

        var colpath = Engine.find(lmCollectibles, function(item) {
            return item.typeRole === collectibleType })

        component = Qt.createComponent(colpath.pathRole);

        collectibleObject = component.createObject(collectibleFactory,
                                                   {"x": roundedValue["x"],
                                                       "y": roundedValue["y"],
                                                       "world": world
                                                   });
        if (collectibleObject == null) {
            // Error Handling
            console.log("Error creating collectible!", component.errorString());
        }

        return collectibleObject
    }
}
