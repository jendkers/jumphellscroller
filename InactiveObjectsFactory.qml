import QtQuick 2.9

import Box2D 2.0

import "MyEngine.js" as Engine

import "Platforms" as Platforms

Item {
    id: inactiveObjectsFactory

    anchors.fill: parent

    property World world

    property var component
    property var platform

    property real windowWidth: 0
    property real windowHeight: 0

    property alias platforms: objects
    property alias model: lmObjects

    QtObject {
        id: objects
        property string block: "InactiveObjects/BlockBackground.qml"
    }

    ListModel {
        id: lmObjects
        ListElement { typeRole: "block_background"; pathRole: "InactiveObjects/BlockBackground.qml" }
    }

    function createObject(x, y, wantedWidth, wantedHeight, objectType) {
        var roundedValue = Engine.getRoundedGridValue(x, y, app.gridXValue, app.gridYValue)

        var colpath = Engine.find(lmObjects, function(item) {
            return item.typeRole === objectType })

        component = Qt.createComponent(colpath.pathRole);
        platform = component.createObject(inactiveObjectsFactory,
                                          {"x": roundedValue["x"],
                                           "y": roundedValue["y"],
                                           "wantedWidth": wantedWidth,
                                           "wantedHeight": wantedHeight,
                                           "world": world
                                          });
        if (platform == null) {
            // Error Handling
            console.log("Error creating inactive object!", component.errorString());
        } else {

        }

        return platform
    }
}
