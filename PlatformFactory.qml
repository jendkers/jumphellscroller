import QtQuick 2.9

import Box2D 2.0

import "MyEngine.js" as Engine

import "Platforms" as Platforms

Item {
    id: platformFactory

    anchors.fill: parent

    property World world

    property var component
    property var platform

    property real windowWidth: 0
    property real windowHeight: 0

    property alias platforms: platformsObj
    property alias model: lmPlatforms

    QtObject {
        id: platformsObj
        property string grass: "Platforms/GrassPlatform.qml"
        property string block: "Platforms/BlockPlatform.qml"
    }

    ListModel {
        id: lmPlatforms
        ListElement { typeRole: "grass"; pathRole: "Platforms/GrassPlatform.qml" }
        ListElement { typeRole: "block"; pathRole: "Platforms/BlockPlatform.qml" }
    }

    function getRandomArbitrary(min, max)
    {
        return Math.random() * (max - min) + min;
    }

    function createPlatform(x, y, wantedWidth, wantedHeight, platformType) {
        var roundedValue = Engine.getRoundedGridValue(x, y, app.gridXValue, app.gridYValue)

        var colpath = Engine.find(lmPlatforms, function(item) {
            return item.typeRole === platformType })

        component = Qt.createComponent(colpath.pathRole);
        platform = component.createObject(platformFactory,
                                          {"x": roundedValue["x"],
                                           "y": roundedValue["y"],
                                           "wantedWidth": wantedWidth,
                                           "wantedHeight": wantedHeight,
                                           "world": world
                                          });
        if (platform == null) {
            // Error Handling
            console.log("Error creating platform!", component.errorString());
        } else {

        }

        return platform
    }

    /**
      * @brief Moves all created and existing platforms
      */
    function updatePlatformsX(platformObjectName) {
        for (var i=0 ; i<children.length ; i++) {
            var item = platformFactory.children[i];
            if (item.objectName === platformObjectName) {
                var x = item.x;
                item.x = -item.width;
            }
        }
    }
}
