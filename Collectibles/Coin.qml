import QtQuick 2.12

import Box2D 2.0

Collectible {
    id: coin

    width: 32
    height: 32

    type: "coin"

    Body {
        id: bodyObj

        target: coin
        bodyType: Body.Static

        world: coin.world

        fixedRotation: true

        Circle {
            id: box

            radius: coin.width / 2

            objectName: coin.objectName

            sensor: true

            onBeginContact: {
                if (other.objectName == "player") {
                    animCatch.start()
                }
            }
        }
    }

    PropertyAnimation {
        id: animCatch

        target: coin
        properties: "y"
        duration: 500
        from: coin.y
        to: coin.y - 60
        easing.type: Easing.OutCubic
        onStopped: coin.destroy()
    }

    AnimatedSprite {
        id: imgCoin

        anchors.centerIn: parent

        source: "qrc:///collectible/graphics/platform_gfx/tiles/food.png"
        frameWidth: 32
        frameHeight: 32
        frameCount: 5
        frameRate: 12
        interpolate: false
        running: true
        loops: AnimatedSprite.Infinite
    }
}
