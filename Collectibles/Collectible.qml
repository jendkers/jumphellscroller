import QtQuick 2.0

import Box2D 2.0

Item {
    id: collectible

    objectName: "collectible"

    property World world

    property string type
}
