import QtQuick 2.12

import Box2D 2.0

Item {
    id: wallSensor

    width: 50
    height: 100

    property World world

    property alias boxObj: box

    property alias active: bodyObj.active

    signal contactWithPlayer
    signal endContact

    Body {
        id: bodyObj

        target: wallSensor
        world: wallSensor.world

        Box {
            id: box

            width: wallSensor.width
            height: wallSensor.height

            objectName: wallSensor.objectName

            sensor: true

            onBeginContact: {
                if (other.objectName == "player") {
                    wallSensor.contactWithPlayer()
                }
            }

            onEndContact: {
                if (other.objectName == "player") {
                    wallSensor.endContact()
                }
            }
        }
    }
}

