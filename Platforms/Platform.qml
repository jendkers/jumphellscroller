import QtQuick 2.12

import Box2D 2.0

/**
 * @brief Base item for platforms in game
 *
 * Example use of this item in new QML item:
 *  import QtQuick 2.12
 *
 *   Platform {
 *       id: platform
 *
 *       forceImageWidth: 32
 *       forceImageHeight: 32
 *
 *       flowAnchors.horizontalCenter: platform.horizontalCenter
 *       flowAnchors.top: platform.top
 *       flowAnchors.topMargin: -3
 *
 *       imageSource: "qrc:///env/graphics/platform_gfx/tiles/block2.png"
 *   }
 *
 * This item will be destroyed when x < -platform.width
 */

Item {
    id: platform

    width: 200
    height: 32

    // Box have the same objectName used to recognize which object collides with
    objectName: "platform"

    property string type

    /** alias to World object of platform body */
    property alias world: bodyObj.world

    /** alias to platform body Box2D object */
    property alias body: bodyObj

    /** this property is set by PlatformFactory, width of platform will be
        automaticly updated when wantedWidth is set */
    property int wantedWidth: 1

    property int wantedHeight: 1

    /** url to single image of platform */
    property url imageSource

    /** overrides size of image from imageSource in px */
    property int forceImageWidth: 32

    /** overrides size of image from imageSource in px */
    property int forceImageHeight: 32

    /** type:Anchors alias to images Flow anchors */
    property alias flowAnchors: flowImages.anchors

    /** type:real alias to spacing property of Flow with images */
    property alias imagesSpacing: flowImages.spacing

    Body {
        id: bodyObj

        target: platform

        Box {
            id: box

            width: platform.width
            height: platform.height

            objectName: platform.objectName

            onBeginContact: {
                if (other.objectName == "") {

                }
            }

            onEndContact: {
            }
        }
    }

    Flow {
        id: flowImages

        width: platform.forceImageWidth * fixedWidth
        height: platform.forceImageHeight * fixedHeight

        onWidthChanged: platform.width = width
        onHeightChanged: platform.height = height

        /** fixed width cropped to forceImageWidth */
        property int fixedWidth: Math.abs(((wantedWidth / platform.forceImageWidth)))
        property int fixedHeight: Math.abs(((wantedHeight / platform.forceImageHeight)))

        /** how much single blocks will be displayed */
        property int modelIntValue: fixedHeight <= 0
                                    ? fixedWidth
                                    : fixedWidth
                                        * fixedHeight

        Repeater {
            model: flowImages.modelIntValue == 0
                   ? 1
                   : flowImages.modelIntValue

            Image {
                source: platform.imageSource
                sourceSize.width: platform.forceImageWidth
                sourceSize.height: platform.forceImageHeight
            }
        }
    }
}

