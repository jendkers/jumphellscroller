import QtQuick 2.12

Platform {
    id: platform

    type: "grass"

    forceImageWidth: 26
    forceImageHeight: 26

    imageSource: "qrc:///env/graphics/tall_grass.png"

    imagesSpacing: -2

    flowAnchors {
        horizontalCenter: platform.horizontalCenter
        bottom: platform.top
        bottomMargin: -15
    }

    Rectangle {
        id: ground

        width: parent.width
        height: parent.height

        radius: 5

        color: "darkgreen"

        anchors.bottom: parent.bottom
    }

    Behavior on x {
        NumberAnimation {
            duration: 5000
        }
    }
}
