import QtQuick 2.12

Platform {
    id: platform

    type: "block"

    forceImageWidth: 32
    forceImageHeight: 32

    flowAnchors.horizontalCenter: platform.horizontalCenter
    flowAnchors.top: platform.top
//    flowAnchors.topMargin: -2

    imageSource: "qrc:///env/graphics/platform_gfx/tiles/block2.png"
}
