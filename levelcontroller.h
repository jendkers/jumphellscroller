#ifndef LEVELCONTROLLER_H
#define LEVELCONTROLLER_H

#include "commontools.h"

#include <QObject>
#include <QJsonObject>

#include <QDebug>

class LevelController : public QObject
{
    Q_OBJECT
public:
    explicit LevelController(QObject *parent = nullptr);

    Q_INVOKABLE void saveLevel(const QJsonObject &levelData);
    Q_INVOKABLE QJsonObject loadLevel(const int levelNumber);

signals:

public slots:
};

#endif // LEVELCONTROLLER_H
