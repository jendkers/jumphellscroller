#include "commontools.h"

QString CommonTools::readTextFile(const QString &filePath)
{
    QFile file(filePath);
    QString result;

    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        result = QString(file.readAll());
        file.close();
    } else {
        qDebug() << "Error: can't open file: " << filePath << file.errorString();
    }

    return result;
}

QString CommonTools::returnUrlFromString(const QString &string)
{
    QRegularExpression regexp;
    regexp.setPattern("((?:https?|ftp)://\\S+)");

    auto match = regexp.match(string);

    if (match.captured(0).isEmpty() == false)
        return match.captured(0);
    else
        return "";
}

bool CommonTools::saveQJsonObjectToFile(const QString &path, const QJsonObject &jsonObj)
{
    QFile saveFile(path);

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return false;
    }

    QJsonDocument saveDoc(jsonObj);
    saveFile.write(saveDoc.toJson());

    return true;
}

QJsonObject CommonTools::loadQJsonObjectFromFile(const QString &path)
{
    QFile loadFile(path);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Couldn't open file." + loadFile.errorString();
        return QJsonObject();
    }

    auto json = QJsonDocument::fromJson(loadFile.readAll());

    return json.object();
}

