import QtQuick 2.9

import "../Platforms"

BlockPlatform {
    type: "block_background"

    body.active: false

    Rectangle {
        anchors.fill: parent

        color: "black"
        opacity: 0.55
    }
}
