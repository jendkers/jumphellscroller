#include "levelcontroller.h"

LevelController::LevelController(QObject *parent) : QObject(parent)
{

}

void LevelController::saveLevel(const QJsonObject &levelData)
{
    CommonTools::saveQJsonObjectToFile("level1.json", levelData);
}

QJsonObject LevelController::loadLevel(const int levelNumber)
{
    return CommonTools::loadQJsonObjectFromFile("level" + QString::number(levelNumber) + ".json");
}
