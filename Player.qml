import QtQuick 2.12

import Box2D 2.0

Item {
    id: player

    width: 32
    height: 40

    objectName: "player"

    property alias world: bodyObj.world
    property alias body: bodyObj
    property alias box: boxObj

    /** this property is a name of folder with graphics for player */
    property string heroTheme: ""

    property bool isJumpKeyPressed: false
    property int jumpTriggerCount: 0
    property int maxJumpTriggerCount: 4

    /** used to rotate player sprites */
    property int zRotation: 0

    /** player body actual velocity in X axis */
    property real xVelocity: 0

    signal keyUpPressed(var event)
    signal keyUpReleased(var event)
    signal keyRight()
    signal keyLeft()

    transform: Rotation { origin.x: width/2; origin.y: height/2; axis { x: 0; y: 1; z: 0 } angle: zRotation }

    function beginContactWith(other)
    {
        if (other.objectName == "platform") {
            player.state = playerStates.running
        }
    }

    function endContactWith(other)
    {
        if (other.objectName == "platform" && !player.isJumpKeyPressed) {
            player.state = playerStates.falling
        }
    }

    onKeyRight: {
        move("right")
        zRotation = 0
    }

    onKeyLeft: {
        move("left")
        zRotation = 180
    }

    onKeyUpPressed: {
        if (player.isJumpKeyPressed == false) {
            if (player.state === playerStates.running) {
                player.isJumpKeyPressed = true

                if (body.getLinearVelocityFromLocalPoint(Qt.point(0,0)).y >= -1) {
                    player.state = playerStates.jumping
                    jumpTriggerCount = 0
                    timerJumpForce.restart()
                }
            }
        } else {
            event.accepted = false
        }
    }
    onKeyUpReleased: {
        if (player.isJumpKeyPressed == true) {
            player.isJumpKeyPressed = false
        }
    }

    onIsJumpKeyPressedChanged: isJumpKeyPressed
                               ? 0
                               : jumpTriggerCount = 0
    Timer {
        id: timerJumpForce
        repeat: true
        interval: 50
        triggeredOnStart: true
        onTriggered: {
            if (jumpTriggerCount < maxJumpTriggerCount && isJumpKeyPressed) {
                jumpTriggerCount++
                body.applyForceToCenter(Qt.point(0, -11), Qt.point(0,0))
            } else {
                timerJumpForce.stop()
            }
        }
    }


    /**
     * Function moves player box2d object, based on applyLinearVelocity
     * @param type:string direction
     */
    function move(direction) {
        xVelocity = body.getLinearVelocityFromLocalPoint(body.getLocalCenter()).x

        var wantedVelocity = 7
        var vel
        var movePoint

        if (direction === "left") {
            wantedVelocity = -wantedVelocity
            vel = wantedVelocity - xVelocity
            if (vel < wantedVelocity)
                vel = wantedVelocity
        } else if (direction === "right") {
            vel = wantedVelocity - xVelocity
            if (vel < 0)
                vel = 0
        }

        console.log("vel: ", vel, xVelocity)

        movePoint = Qt.point(body.getMass()*vel, 0)
        body.applyLinearImpulse(movePoint, body.getWorldCenter())//player.x+=5
    }

    QtObject {
        id: playerStates
        property string standing: "STATE_STANDING"
        property string running: "STATE_RUNNING"
        property string jumping: "STATE_JUMPING"
        property string falling: "STATE_FALLING"
    }

    state: playerStates.falling // starting state
    states: [
        State {
            name: playerStates.standing
            PropertyChanges {
                target: animations.animStanding
                visible: true
                running: true
            }
            PropertyChanges {
                target: animations.animFalling
                visible: false
                running: false
            }
            PropertyChanges {
                target: animations.animRunning
                visible: false
                running: false
            }
            PropertyChanges {
                target: animations.animJumping
                visible: false
                running: false
            }
        },
        State {
            name: playerStates.running
            PropertyChanges {
                target: animations.animStanding
                visible: false
                running: false
            }
            PropertyChanges {
                target: imgFalling
                visible: false
                running: false
            }
            PropertyChanges {
                target: animations.animRunning
                visible: true
                running: true
            }
            StateChangeScript {
                script: animations.animRunning.restart()
            }
            PropertyChanges {
                target: animations.animJumping
                visible: false
                running: false
            }
        },
        State {
            name: playerStates.jumping
            PropertyChanges {
                target: animations.animStanding
                visible: false
                running: false
            }
            PropertyChanges {
                target: animations.animFalling
                visible: false
                running: false
            }
            PropertyChanges {
                target: animations.animRunning
                visible: false
                running: false
            }
            StateChangeScript {
                script: animations.animJumping.restart()
            }
            PropertyChanges {
                target: animations.animJumping
                visible: true
                running: true
            }
        },
        State {
            name: playerStates.falling
            PropertyChanges {
                target: animations.animStanding
                visible: false
                running: false
            }
            PropertyChanges {
                target: animations.animFalling
                visible: true
                running: true
            }
            PropertyChanges {
                target: animations.animRunning
                visible: false
                running: false
            }
            PropertyChanges {
                target: animations.animJumping
                visible: false
                running: false
            }
        }
    ]

    Body {
        id: bodyObj

        target: player
        bodyType: Body.Dynamic
        sleepingAllowed: false
        fixedRotation: true
        linearDamping: 0.1
        angularDamping: 0.1
        gravityScale: 1.5

        Box {
            id: boxObj

            width: player.width / 1.5
            height: player.height

            density: 0.07
            friction: 0.07
            restitution: 0.07

            objectName: player.objectName

            onBeginContact: beginContactWith(other)

            onEndContact: endContactWith(other)
        }
    }

    Item {
        id: animations

        width: parent.width
        height: parent.height

        anchors {
            top: parent.top
            topMargin: 2
        }

        property alias animStanding: imgStanding
        property alias animRunning: imgRunning
        property alias animJumping: imgJumping
        property alias animFalling: imgFalling

        AnimatedSprite {
            id: imgStanding

            anchors.bottom: parent.bottom

            source: "qrc:///player/graphics/platform_gfx/" + heroTheme + "/idle.png"
            frameWidth: 32
            frameHeight: 64
            frameCount: 1
        }

        AnimatedSprite {
            id: imgRunning

            anchors.bottom: parent.bottom

            source: "qrc:///player/graphics/platform_gfx/" + heroTheme + "/run.png"
            frameWidth: 32
            frameHeight: 64
            frameCount: 6
            frameRate: 12
            interpolate: false
            loops: AnimatedSprite.Infinite
        }

        AnimatedSprite {
            id: imgJumping

            anchors.bottom: parent.bottom

            source: "qrc:///player/graphics/platform_gfx/" + heroTheme + "/jump.png"
            frameWidth: 32
            frameHeight: 64
            frameCount: 3
            frameRate: 1
            interpolate: false
            loops: AnimatedSprite.Infinite
        }

        AnimatedSprite {
            id: imgFalling

            anchors.bottom: parent.bottom

            source: "qrc:///player/graphics/platform_gfx/" + heroTheme + "/pivot.png"
            frameWidth: 32
            frameHeight: 64
            frameCount: 1
        }
    }
}
