.pragma library

/**
 * @brief Returns object with x and y position rounded down to
          gridX and gridY values
 * @param type:real x
 * @param type:real y
 * @param type:int gridX
 * @param type:int gridY
 */
function getRoundedGridValue(x, y, gridX, gridY)
{
    var valueX = (x / gridX).toFixed();
    var valueY = (y / gridY).toFixed();
    valueX *= gridX
    valueY *= gridY

    return { "x" : valueX, "y" : valueY }
}

/**
 * @brief Searchs ListModel type object to look for item that completes
   given criteria
 * @param type:ListModel model
 * @param type:function criteria function that has criteria
 */
function find(model, criteria) {
    for (var i = 0; i < model.count; ++i)
        if (criteria(model.get(i)))
            return model.get(i)
    return null
}

function printChildrens(childrenArray)
{
    for (var i=0 ; i<childrenArray.length; i++) {
        var item = childrenArray[i]
        console.log("Children: ", item, "name: ", item.objectName, "pos: ", item.x, item.y, "width: ", item.width, "height: ", item.height)
    }
}

/**
 * @brief Returns object with level data informations such as platforms
 positions etc.
   Function reads children arrays data of given QML Item objects and returns readed
   data as QJsonObject. For now it read only platforms and collectibles.
 * @param type:list platformsArray platforms factory children array
 * @param type:list collectibleArray collectibles factory children array
 */
function getLevelData(level, player, inactiveObjectsArray, platformsArray, collectibleArray)
{
    var levelData = {}
    levelData["width"] = level.contentWidth
    levelData["height"] = level.contentHeight

    var playerData = {}
    playerData["posX"] = player.x
    playerData["posY"] = player.y

    var inactiveObjectsData = []
    for (var i=0 ; i<inactiveObjectsArray.length; i++) {
        var item = inactiveObjectsArray[i]

        inactiveObjectsData[i] = {
            "posX" : item.x,
            "posY" : item.y,
            "name" : item.objectName,
            "width" : item.width,
            "wantedWidth" : item.wantedWidth,
            "wantedHeight" : item.wantedHeigth,
            "height" : item.height,
            "type" : item.type }
    }

    var platformsData = []
    for (i=0 ; i<platformsArray.length; i++) {
        item = platformsArray[i]

        platformsData[i] = {
            "posX" : item.x,
            "posY" : item.y,
            "name" : item.objectName,
            "width" : item.width,
            "wantedWidth" : item.wantedWidth,
            "wantedHeight" : item.wantedHeigth,
            "height" : item.height,
            "type" : item.type }
    }

    var collectiblesData = []
    for (i=0 ; i<collectibleArray.length; i++) {
        item = collectibleArray[i]

        collectiblesData[i] = {
            "posX" : item.x,
            "posY" : item.y,
            "name" : item.objectName,
            "width" : item.width,
            "height" : item.height,
            "type" : item.type}
    }

    return {
        "level" : levelData,
        "player" : playerData,
        "inactiveObjects" : inactiveObjectsData,
        "platforms" : platformsData,
        "collectibles" : collectiblesData }
}

function buildLevel(levelData, levelObj, playerObj, inactiveObjectsFactory, platformFactory, collectibleFactory)
{
    var item
    var i

    // creating inactive objects on level
    var inactiveObjectsArray = levelData["inactiveObjects"]
    for (var i=0 ; i<inactiveObjectsArray.length; i++) {
        item = inactiveObjectsArray[i]

        inactiveObjectsFactory.createObject(item["posX"],
                                          item["posY"],
                                          item["wantedWidth"],
                                          item["height"],
                                          item["type"])
    }

    // creating platforms on level
    var platformsArray = levelData["platforms"]
    for (i=0 ; i<platformsArray.length; i++) {
        item = platformsArray[i]

        platformFactory.createPlatform(item["posX"],
                                          item["posY"],
                                          item["wantedWidth"],
                                          item["height"],
                                          item["type"])
    }

    // creating collectibles on level
    var collectibleArray = levelData["collectibles"]
    for (i=0 ; i<collectibleArray.length; i++) {
        item = collectibleArray[i]

        collectibleFactory.createCollectable(item["posX"],
                                             item["posY"],
                                             item["type"])
    }

    // levelObj passing now

    // placing player and activating it
    var playerData = levelData["player"]
    playerObj.x = playerData["posX"]
    playerObj.y = playerData["posY"]
    playerObj.visible = true
    playerObj.body.active = true
}

function resetLevel(levelObj, playerObj, inactiveObjectsFactoryArray, platformFactoryArray, collectibleFactoryArray)
{
    var item
    var i

    // creating inactive objects on level
    for (i=0 ; i<inactiveObjectsFactoryArray.length; i++) {
        inactiveObjectsFactoryArray[i].destroy()
    }

    // creating inactive objects on level
    for (i=0 ; i<platformFactoryArray.length; i++) {
        platformFactoryArray[i].destroy()
    }

    // creating inactive objects on level
    for (i=0 ; i<collectibleFactoryArray.length; i++) {
        collectibleFactoryArray[i].destroy()
    }

    // levelObj passing now

    // reseting player
    playerObj.x = -100
    playerObj.y = -100
    playerObj.visible = false
    playerObj.body.active = false
}
