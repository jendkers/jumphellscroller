import QtQuick 2.12

Item {
    id: button

    implicitWidth: 100
    implicitHeight: 50

    property bool interactive: true
    property bool isSelected: false

    property alias colorOfBackground: btnBackground.color
    property alias colorOfText: btnText.color

    property alias text: btnText.text

    signal clicked()

    signal selected()
    signal unSelected()

    states: [
        State {
            name: "STATE_CLICKED"
            when: isSelected == true
            PropertyChanges {
                target: container
                scale: 0.8
            }
        },
        State {
            name: "STATE_UNCLICKED"
            when: isSelected == false
            PropertyChanges {
                target: container
                scale: 1
            }
        }
    ]

    Behavior on scale { ScaleAnimator { duration: 300 } }

    Item {
        id: container

        width: parent.width
        height: parent.height

        Rectangle {
            id: btnBackground

            width: parent.width
            height: parent.height

            color: "white"
        }

        Text {
            id: btnText

            anchors.centerIn: parent

            color: "black"
        }
    }

    MouseArea {
        anchors.fill: parent
        enabled: interactive

        onClicked: {
            parent.clicked()
            if (isSelected === true) {
                isSelected = false
                unSelected()
            } else {
                isSelected = true
                selected()
            }
        }
    }
}
