#ifndef COMMONTOOLS_H
#define COMMONTOOLS_H

#include <QObject>

#include <QObject>
#include <QUrl>
#include <QVariantList>
#include <QVariant>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QIODevice>
#include <QRegularExpression>
#include <QDebug>

class CommonTools : public QObject
{
    Q_OBJECT
public:
    explicit CommonTools(QObject *parent = nullptr);

    QString readTextFile(const QString &filePath);
    QString returnUrlFromString(const QString &string);

    static bool saveQJsonObjectToFile(const QString &path, const QJsonObject &jsonObj);
    static QJsonObject loadQJsonObjectFromFile(const QString &path);

signals:

public slots:
};

#endif // COMMONTOOLS_H
